% Concevoir une bibliothèque logicielle
%
% Auteur  : Gregory DAVID
% Inspiration : "Program Library HOWTO" David A. Wheeler v.1.36, 15 may 2010
%%

\documentclass[french]{beamer}
\usepackage{morewrites}
\usepackage{bashful}

\lstdefinestyle{bashfulStdout}{%
  basicstyle=\ttfamily\tiny,%
  keywords={},%
  showstringspaces=false%
}
\lstdefinestyle{bashfulScript}{%
  basicstyle=\ttfamily\tiny,%
  keywords={},%
  showstringspaces=false%
}

\usepackage{csquotes}
\usepackage[backend=bibtex8,backref=true,defernumbers=true]{biblatex}
\addbibresource{\jobname.bib}
\defbibcheck{online}{\iffieldundef{url}{\skipentry}{}}
\defbibcheck{notonline}{\iffieldundef{url}{}{\skipentry}}

\usepackage{style/beamer_layout}
\usepackage{style/beamer_commands}
\usepackage{style/glossaire}
\usepackage{style/graphviz}

\usetheme{Luebeck}
\usecolortheme{seagull}
\newcommand{\MONTITRE}{Conception d'une bibliothèque logicielle}
\newcommand{\MONSOUSTITRE}{en langage C}
\newcommand{\DISCIPLINE}{\gls{SLAMcinq}}

\title{\MONTITRE}
\subtitle{\MONSOUSTITRE}
\author[\REVISIONS, \MOI]{\MOI}
\institute{\DISCIPLINE}
\date{\today}

\begin{document}
% Nettoyage du code source d'exemple
\bash
cd arithmetique
make clean
\END

\maketitle

\section{Définitions}
\begin{frame}{Bibliothèque logicielle}
    \begin{itemize}
        \item Conteneur
        \item Module
        \item \textit{Plugin}
        \item Mise à disposition de code pour plusieurs programmes
    \end{itemize}
\end{frame}

\begin{frame}{Typologie}
    \begin{itemize}
        \item Bibliothèque statique (\emph{static library} :
        \texttt{.a})
        \item Bibliothèque dynamique (\emph{shared library} :
        \texttt{.so})
        \item Bibliothèque chargée dynamiquement (\emph{dynamically
          loaded library} : \texttt{DL})
    \end{itemize}
\end{frame}

\section{Structure du projet exemple}
\label{sec:projet.structure}
\begin{frame}[containsverbatim]
\bash[stdout]
tree -P "*.h|*.c" arithmetique
\END
\end{frame}

\section{Bibliothèque statique -- static library}
\label{sec:lib.static}
\begin{frame}{\emph{static library}}
    \begin{itemize}
        \item Porte l'extension \texttt{.a}
        \item Collection d'objets binaires \texttt{.o} archivés avec
        l'utilitaire \texttt{ar}
        \item Encapsulée complètement dans le binaire du programme qui
        la lie
        \item Chargement dynamique impossible
    \end{itemize}
\end{frame}

\subsection{Principe}
\label{sec:lib.static.principe}
\begin{frame}
    \digraphBeamer[width=10cm]{dot}{compilation_static_library}{%
      graph [splines=spline];%
      node [shape=note];%
      mainC [label="main.c"];%
      mainO [label="main.o"];%
      mainS [label="main.s"];%
      programme [label="programme"];%
      additionC [label="addition.c"];%
      additionO [label="addition.o"];%
      additionS [label="addition.s"];%
      soustractionC [label="soustraction.c"];%
      soustractionO [label="soustraction.o"];%
      soustractionS [label="soustraction.s"];%
      multiplicationC [label="multiplication.c"];%
      multiplicationO [label="multiplication.o"];%
      multiplicationS [label="multiplication.s"];%
      divisionC [label="division.c"];%
      divisionO [label="division.o"];%
      divisionS [label="division.s"];%
      libRealName [label="libma_bib_arithmetique.a"];%
      subgraph clusterHeaders{%
        graph [label="Declarations, fichiers
        en-tete",labelloc=t,labeljust=r,fontsize=20];%
        fonctionsH [label="fonctions.h"];%
      };%
      subgraph clusterLibPreProc{%
        graph [label="Pre-Processing
        library",labelloc=t,labeljust=r,fontsize=20];%
        {additionC soustractionC multiplicationC divisionC} ->
        fonctionsH [dir=both,style=dotted];%
        additionC -> additionS;%
        soustractionC -> soustractionS;%
        multiplicationC -> multiplicationS;%
        divisionC -> divisionS;%
      };%
      subgraph clusterProgPreProc{%
        graph [label="Pre-Processing du
        programme",labelloc=t,labeljust=r,fontsize=20];%
        mainC -> fonctionsH [dir=both,style=dotted];%
        mainC -> mainS;%
      };%
      subgraph clusterLibCompiler{%
        graph [label="Compilation de la
        library",labelloc=t,labeljust=r,fontsize=20];%
        additionS -> additionO;%
        soustractionS -> soustractionO;%
        multiplicationS -> multiplicationO;%
        divisionS -> divisionO;%
      };%
      subgraph clusterProgCompiler{%
        graph [label="Compilation du
        programme",labelloc=t,labeljust=r,fontsize=20];%
        mainS -> mainO;%
      };%
      subgraph clusterLibLinker{%
        graph [label="Edition des liens de la
        library",labelloc=b,labeljust=r,fontsize=20];%
        {additionO soustractionO multiplicationO divisionO} ->
        libRealName;%
      };%
      subgraph clusterProgLinker{%
        graph [label="Edition des liens du
        programme",labelloc=b,labeljust=r,fontsize=20];%
        mainO -> programme;%
        libRealName -> programme [style=bold, label="encapsulation"];%
      };%
    }{gph:label}%
\end{frame}

\subsection{Nomenclature}
\label{sec:lib.static.nomenclature}
\begin{frame}
    \begin{itemize}
        \item le \emph{linker name} : une suite de caractères ASCII
        \item le \emph{real name} :
        \begin{itemize}
            \item le préfixe \texttt{lib}
            \item le \emph{linker name}
            \item le suffixe \texttt{.a}
        \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Exemple de construction}
\label{sec:lib.static.exemple}
\begin{frame}[containsverbatim]
    Pour cet exemple :
    \begin{description}
        \item[\emph{linker name}] $=$ \texttt{arithmetique\_library}
        \item[\emph{realname}] $=$ \texttt{libarithmetique\_library.a}
    \end{description}
\lstset{caption={Construction de la bibliothèque statique}}
\bash[script]
cd arithmetique/lib
gcc -c -Wall -I../include addition.c soustraction.c multiplication.c division.c
ar rcs libarithmetique_library.a addition.o soustraction.o multiplication.o division.o
\END

\lstset{caption={Construction du programme encapsulant la bibliothèque statique}}
\bash[script]
cd arithmetique/src
gcc -o ../bin/arithmetique main.c -I../include -L../lib -larithmetique_library
\END
\end{frame}

\subsection{Vérification}
\label{sec:lib.static.verification}
\begin{frame}[containsverbatim]{Contenu de la \emph{static library}}
\lstset{caption={Vérification du contenu de la bibliothèque}}
\bash[script]
readelf -c arithmetique/lib/libarithmetique_library.a
\END

\lstset{caption={Sortie de \texttt{readelf}}}
\bash[stdout]
LANG='' readelf -c arithmetique/lib/libarithmetique_library.a
\END
\end{frame}
\begin{frame}[containsverbatim]{Encapsulation de la \emph{static library} dans le programme}
\lstset{caption={Vérification du contenu de la bibliothèque}}
\bash[script]
objdump -a arithmetique/lib/libarithmetique_library.a
\END

\lstset{caption={Sortie de \texttt{objdump}}}
\bash[stdout]
objdump -a arithmetique/lib/libarithmetique_library.a | sed '1,2d'
\END
\end{frame}%$

\subsection{Makefile}
\label{sec:makefilestaticlib}
\begin{frame}[containsverbatim]{\texttt{Makefile} de construction de la \emph{static library}}
Le fichier est situé dans le chemin \texttt{arithmetique/lib/Makefile}
    \resizebox{7.5cm}{!}{
      \lstinputlisting[language={[gnu]make},linerange={5-10,18-23,30-31,41-42,46-47,56-57,63-65,69-71},emptylines=0]{arithmetique/lib/Makefile}
    }
\end{frame}
\begin{frame}[containsverbatim]
    \texttt{Makefile} du programme utilisant la \emph{static library},
    situé dans le chemin \texttt{arithmetique/src/Makefile}
    \begin{columns}[t]
        \begin{column}{5cm}
            \lstinputlisting[basicstyle={\ttfamily\tiny},language={[gnu]make},linerange={1-21},emptylines=0]{arithmetique/src/Makefile}
        \end{column}
        \begin{column}{5cm}
            \lstinputlisting[firstnumber=last,basicstyle={\ttfamily\tiny},language={[gnu]make},linerange={26},emptylines=0]{arithmetique/src/Makefile}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}[containsverbatim]
    Exécution du binaire, la \emph{static library} y est incorporée :
\begin{lstlisting}
arithmetique/bin/arithmetique
\end{lstlisting}
Sortie standard du programme :
% \bash[stdout]
% arithmetique/bin/arithmetique
% \END
\end{frame}

\section{Bibliothèque dynamique}
\label{sec:dynamicLib}
\begin{frame}{Bibliothèque dynamique -- shared library}
    \begin{itemize}
        \item La \emph{shared library} est un assemblage spécifique
        des fichiers objets \texttt{.o}
        \item Chargement au démarrage du programme qui la lie
        \item L'option \texttt{-Lchemin} lors de l'édition des liens
        spécifie le chemin relatif ou absolu vers la \emph{shared
          library}
        \item \texttt{LD\_LIBRARY\_PATH} doit contenir le chemin de la
        \emph{shared library} pour pouvoir exécuter un programme qui
        la lie
        \item Au moins 3 fichiers : \texttt{libtruc.so},
        \texttt{libtruc.so.1} et \texttt{libtruc.so.1.4.322}
    \end{itemize}
\end{frame}
\begin{frame}{Principe -- shared library}
    \digraphBeamer[width=10cm]{dot}{compilation_shared_library}{%
      graph [splines=spline];%
      node [shape=note];%
      mainC [label="main.c"];%
      mainO [label="main.o"];%
      mainS [label="main.s"];%
      programme [label="programme"];%
      additionC [label="addition.c"];%
      additionO [label="addition.o"];%
      additionS [label="addition.s"];%
      soustractionC [label="soustraction.c"];%
      soustractionO [label="soustraction.o"];%
      soustractionS [label="soustraction.s"];%
      multiplicationC [label="multiplication.c"];%
      multiplicationO [label="multiplication.o"];%
      multiplicationS [label="multiplication.s"];%
      divisionC [label="division.c"];%
      divisionO [label="division.o"];%
      divisionS [label="division.s"];%
      libRealName [label="libma_bib_arithmetique.so.1.0.1"];%
      libSoname [label="libma_bib_arithmetique.so.1"];%
      libLinkerName [label="libma_bib_arithmetique.so"];%
      subgraph clusterHeaders{%
        graph [label="Declarations, fichiers
        en-tete",labelloc=t,labeljust=r,fontsize=20];%
        fonctionsH [label="fonctions.h"];%
      };%
      subgraph clusterLibPreProc{%
        graph [label="Pre-Processing
        library",labelloc=t,labeljust=r,fontsize=20];%
        {additionC soustractionC multiplicationC divisionC} ->
        fonctionsH [dir=both,style=dotted];%
        additionC -> additionS;%
        soustractionC -> soustractionS;%
        multiplicationC -> multiplicationS;%
        divisionC -> divisionS;%
      };%
      subgraph clusterProgPreProc{%
        graph [label="Pre-Processing
        programme",labelloc=t,labeljust=r,fontsize=20];%
        mainC -> fonctionsH [dir=both,style=dotted];%
        mainC -> mainS;%
      };%
      subgraph clusterLibCompiler{%
        graph [label="Compilation de la
        library",labelloc=t,labeljust=r,fontsize=20];%
        additionS -> additionO;%
        soustractionS -> soustractionO;%
        multiplicationS -> multiplicationO;%
        divisionS -> divisionO;%
      };%
      subgraph clusterProgCompiler{%
        graph [label="Compilation du
        programme",labelloc=t,labeljust=r,fontsize=20];%
        mainS -> mainO;%
      };%
      subgraph clusterLibLinker{%
        graph [label="Edition des liens de la
        library",labelloc=b,labeljust=r,fontsize=20];%
        {additionO soustractionO multiplicationO divisionO} ->
        libRealName;%
      };%
      subgraph clusterProgLinker{%
        graph [label="Edition des liens du
        programme",labelloc=b,labeljust=r,fontsize=20];%
        mainO -> programme;%
        libRealName -> programme [dir=both, style=dotted,
        label="link"];%
      };%
      subgraph clusterSymlink{%
        graph [label="Liens
        symboliques",labelloc=t,labeljust=r,fontsize=20];%
        {libSoname libLinkerName} -> libRealName [style=dashed];%
      };%
    }{gph:label}%
\end{frame}
\begin{frame}{Nomenclature -- shared library}
    La \emph{shared library} est identifiée par 3 noms distincts
    utilisés dans des contextes complémentaires :
    \begin{itemize}
        \item le \emph{linker name} : une suite de caractères ASCII
        \item le \emph{soname} :
        \begin{itemize}
            \item le préfixe \texttt{lib}
            \item le \emph{linker name}
            \item le suffixe \texttt{.so}
            \item un point \texttt{.}
            \item le numéro \emph{majeur}
            (MAJOR) de version (selon la numérotation sémantique des
            versions : voir \href{http://www.semver.org}{semver.org})
            variant au fur et à mesure des changements apportés dans
            la \emph{shared library}
        \end{itemize}
        \item le \emph{realname} ajoute en suffixe au \emph{soname} :
        \begin{itemize}
            \item un point \texttt{.}
            \item le numéro \emph{mineur} (MINOR) de version
            \item le numéro du \emph{correctif} (PATCH) [optionnel]
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[containsverbatim]{Exemple -- shared library}
    \begin{description}
        \item[\emph{linker name}] \texttt{ma\_bib\_arithmetique}
        \item[\emph{real name}]
        \texttt{libma\_bib\_arithmetique.so.1.0.1}
        \item[\emph{soname}] \texttt{libma\_bib\_arithmetique.so.1}
    \end{description}
    \begin{lstlisting}
$ gcc -Wall -shared -fPIC -c addition.c soustraction.c division.c multiplication.c
$ gcc -Wall -shared -fPIC -Wl,-soname=libma_bib_arithmetique.so.1 -o libma_bib_arithmetique.so.1.0.1 addition.o soustraction.o division.o multiplication.o
$ ln -sv libma_bib_arithmetique.so.1.0.1 libma_bib_arithmetique.so
$ ln -sv libma_bib_arithmetique.so.1.0.1 libma_bib_arithmetique.so.1
$ gcc -o programme.shared main.c -L. -lma_bib_arithmetique
$ LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH ./programme.shared
    \end{lstlisting}%$
\end{frame}
\begin{frame}[containsverbatim]{\texttt{Makefile} de construction de la \emph{shared library}}
    Le fichier est situé dans le chemin \texttt{arithmetique/lib/Makefile}
    \begin{columns}[t]
        \begin{column}{5cm}
            \lstinputlisting[basicstyle={\ttfamily\tiny},language={[gnu]make},linerange={1-8,11-20},emptylines=0]{arithmetique/lib/Makefile}
        \end{column}
        \begin{column}{5cm}
            \lstinputlisting[firstnumber=last,basicstyle={\ttfamily\tiny},language={[gnu]make},linerange={22-31,41-42,46-47},emptylines=0]{arithmetique/lib/Makefile}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}[containsverbatim]{\texttt{Makefile} de construction de la \emph{shared library} suite \dots}
    \lstinputlisting[firstnumber=last,basicstyle={\ttfamily\tiny},language={[gnu]make},linerange={48-49,52-55,66-71},emptylines=0]{arithmetique/lib/Makefile}
\end{frame}

\begin{frame}[containsverbatim]
    \texttt{Makefile} du programme utilisant la \emph{shared library},
    situé dans le chemin \texttt{arithmetique/src/Makefile}
    \begin{columns}[t]
        \begin{column}{5cm}
            \lstinputlisting[basicstyle={\ttfamily\tiny},language={[gnu]make},linerange={1-21},emptylines=0]{arithmetique/src/Makefile}
        \end{column}
        \begin{column}{5cm}
            \lstinputlisting[firstnumber=last,basicstyle={\ttfamily\tiny},language={[gnu]make},linerange={26},emptylines=0]{arithmetique/src/Makefile}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}[containsverbatim]
    Exécution du binaire, par redéfinition préalable de la variable
    d'environnement de recherche des \emph{shared libraries} :
\begin{lstlisting}
LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH arithmetique/bin/arithmetique
\end{lstlisting}
Sortie standard du programme :
% \bash[stdout]
% LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH arithmetique/bin/arithmetique
% \END
\end{frame}

\begin{frame}
    \printbibliography
    \nocite{*}
\end{frame}

\begin{frame}{Licence GNU FDL 1.3}
    \begin{quote}
        Copyright \copyright\ \the\year\ \MAILGREGORY.  Permission
        vous est donn\'ee de copier, distribuer et/ou modifier ce
        document selon les termes de la Licence GNU Free Documentation
        License, Version 1.3 ou ult\'erieure publi\'ee par la Free
        Software Foundation ; sans section inalt\'erable, sans texte
        de premi\`ere page de couverture et sans texte de derni\`ere
        page de couverture. Une copie de cette Licence est disponible
        sur l'Internet à l'adresse :
        \url{https://www.gnu.org/licenses/fdl.html}.
    \end{quote}
\end{frame}

\end{document}

